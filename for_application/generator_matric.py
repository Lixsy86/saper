import random
from typing import List, Union, Optional, Any


ACTUAL_MAP = []
BOMB_MAP = []


def map(actual_map: List[List[Union[str, int]]]) -> None:
    for i in range(len(actual_map)):
        for j in range(len(actual_map[i])):
            print(actual_map[i][j], end='  ')
        print()


def string(k: int) -> List[str]:
    mas: List[str] = []
    for i in range(k):
        mas.append('-')
    return mas


def generate_map(k: int, m: int) -> List[List[str]]:
    mas: List[List[str]] = []
    for i in range(m):
        mas.append(string(k))
    return mas


def second_string(k: int) -> List[bool]:
    mas: List[bool] = []
    for i in range(k):
        mas.append(False)
    return mas


def generate_bomb_map(k: int, m: int) -> List[List[bool]]:
    bomb_map: List[List[bool]] = []
    for i in range(m):
        bomb_map.append(second_string(k))
    return bomb_map


def get_random_indexes(matrix: List[List[Any]]) -> (int,int):
    random_i: int = random.randint(0, len(matrix) - 1)
    random_j: int = random.randint(0, len(matrix[random_i]) - 1)
    return random_i, random_j


def add_one_random_bomb(matrix: List[List[bool]]) -> None:
    random_i, random_j = get_random_indexes(matrix)
    matrix[random_i][random_j] = True


def digit_input(data: str) -> (int,int):
    data: List[str] = data.split(',')
    return int(data[0]), int(data[1])


def add_n_random_bomb(bomb_map: List[List[bool]], n: int) -> None:
    while True:
        random_i, random_j = get_random_indexes(bomb_map)
        bomb_map[random_i][random_j] = True
        sum_bomb = 0
        sum_n = n
        for i in range(len(bomb_map)):
            for j in range(len(bomb_map[i])):
                if bomb_map[i][j]:
                    sum_bomb = sum_bomb + 1
        if sum_n == sum_bomb:
            return
